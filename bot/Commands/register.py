from discord.ext import commands
from sqlalchemy import create_engine
from secrets_bot import Secretsdc
from database_bot import Serverdc,Discipline,Calendar
from sqlalchemy.orm import Session
from datetime import datetime


class Register(commands.Cog):
    """Work with Cryptos"""

    secrets = Secretsdc()

    engine = create_engine(f'{secrets.get_engine_sqlalchemy()}')

    
    def __init__(self, bot):
        self.bot = bot

    @commands.command(help = "Comando para fazer o registro do seu Servidor no Banco de dados")
    async def register(self,ctx):
        try:
            Sessionz = Session(bind=self.engine)
            nmserver = ctx.guild.name
            cdserver = ctx.guild.id
            add = Serverdc(ncdserver=cdserver, snmserver=nmserver)
            Sessionz.add(add)
            Sessionz.commit()   
            await ctx.send(f"Servidor registrado")

        except:
            await ctx.send(f"Ops... Ocorreu um erro")
    
    @commands.command(help = "Comando para fazer o registro da Disciplina")
    async def discipline(self,ctx):
        try:
            Sessionz = Session(bind=self.engine)
            cdserver       = ctx.guild.id
            cddiscipline   = ctx.channel.id
            nmdiscipline   = ctx.channel.name

            add = Discipline(ncdserver=cdserver, ncddiscipline=cddiscipline,snmdiscipline=nmdiscipline)
            Sessionz.add(add)
            Sessionz.commit()   
            await ctx.send(f"Disciplina registrada!")

        except:
            await ctx.send(f"Ops... Ocorreu um erro")
    
    @commands.command(help = "Comando para fazer o registro no calendario")
    async def calendar(self,ctx, date):
        try:
            Sessionz = Session(bind=self.engine)
            cddiscipline = ctx.channel.id
            ttest = datetime.strptime(date, '%d/%m/%Y').date()
            testday = None
            cdcalendar = self.select_cdcalendar() + 1
            cdserver = ctx.guild.id

            add = Calendar(ncddiscipline=cddiscipline, dttest=ttest,ntestday=testday,ncdcalendar=cdcalendar,ncdserver=cdserver)
            Sessionz.add(add)
            Sessionz.commit()   
            await ctx.send(f"Data registrada!")

        except:
            await ctx.send(f"Ops... Ocorreu um erro")

    def select_cdcalendar(self):
        Sessionz = Session(bind=self.engine)

        for instance in Sessionz.query(Calendar).order_by(Calendar.ncdcalendar):
            cdcalendar = instance.ncdcalendar

        return cdcalendar

def setup(bot):
    bot.add_cog(Register(bot))
