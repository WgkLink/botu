from sqlalchemy import Column, Integer, String, Date, ForeignKey
from sqlalchemy.orm import declarative_base
from sqlalchemy.sql.schema import ForeignKey

Base = declarative_base()

class Serverdc(Base):

    __tablename__ = 'serverdc'

    ncdserver = Column(Integer, primary_key=True)
    snmserver = Column(String(100))

    def __repr__(self):
        return f"<user(ncdserver{self.ncdserver},snmserver{self.snmserver})>"

class Discipline(Base):

    __tablename__ = 'discipline'

    ncdserver = Column(Integer, ForeignKey(Serverdc.ncdserver))
    ncddiscipline = Column(Integer, primary_key=True)
    snmdiscipline = Column(String(100))
    def __repr__(self):
        return f"<user(ncdserver{self.ncdserver},ncddiscipline{self.ncddiscipline},snmdiscipline{self.snmdiscipline})>"

class Calendar(Base):

    __tablename__ = 'calendar'

    ncddiscipline = Column(Integer,ForeignKey(Discipline.ncddiscipline))
    dttest = Column(Date)
    ntestday = Column(Integer)
    ncdcalendar = Column(Integer,primary_key=True)
    ncdserver = Column(Integer,ForeignKey(Discipline.ncdserver))

    def __repr__(self):
        return f"<user(ncddiscipline{self.ncddiscipline},dttest{self.dttest},ntestday{self.ntestday},ncdcalendar{self.ncdcalendar},ncdserver{self.ncdserver})>"