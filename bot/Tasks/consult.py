import datetime
from sqlalchemy.orm import Session
from discord.ext import commands,tasks
from sqlalchemy import create_engine
from secrets_bot import Secretsdc
from database_bot import Calendar, Discipline

class Consult(commands.Cog):

    secrets = Secretsdc()

    engine = create_engine(f'{secrets.get_engine_sqlalchemy()}')


    def __init__(self, bot):
        self.bot = bot
    
    @commands.Cog.listener()
    async def on_ready(self):
        self.consult_db.start()
    
    @tasks.loop(hours=24)
    async def consult_db(self):
        Sessionz = Session(bind=self.engine)

        dataAtual = current_time()
        for instance in Sessionz.query(Calendar.dttest, Discipline.snmdiscipline, Calendar.ncddiscipline).filter(Calendar.dttest >= dataAtual).join(Discipline, Discipline.ncddiscipline == Calendar.ncddiscipline):
            channel = self.bot.get_channel(instance.ncddiscipline)
            await channel.send(f"Prova/trabalho = {instance.snmdiscipline}, data: {formatDate(instance.dttest)}")

def setup(bot):
    bot.add_cog(Consult(bot))

def current_time():
    now = datetime.datetime.now()
    now = now.strftime("%m/%d/%Y")
    return now

def formatDate(date):
    date = date.strftime("%d/%m/%Y")
    return date
