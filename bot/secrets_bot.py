import json
import boto3
import base64
from botocore.exceptions import ClientError

class Secretsdc:
    
    __token = None
    __database = None
    __engine = None
    __username = None
    __password = None
    __host = None
    __port = None
    __db = None

    def __init__(self):
        self.__database = self.get_secret_aws("arn:aws:secretsmanager:us-east-1:963121599153:secret:database-Jw0fT3","us-east-1")   
        
    def get_secret_aws(self, secret_name, region_name):

        session = boto3.session.Session()
        client = session.client(
            service_name='secretsmanager',
            region_name=region_name
        )

        try:
            get_secret_value_response = client.get_secret_value(
                SecretId=secret_name
            )
        except ClientError as e:
            if e.response['Error']['Code'] == 'DecryptionFailureException':
                raise e
            elif e.response['Error']['Code'] == 'InternalServiceErrorException':
                raise e
            elif e.response['Error']['Code'] == 'InvalidParameterException':
                raise e
            elif e.response['Error']['Code'] == 'InvalidRequestException':
                raise e
            elif e.response['Error']['Code'] == 'ResourceNotFoundException':
                raise e
        else:
            if 'SecretString' in get_secret_value_response:
                self.__engine = json.loads(get_secret_value_response['SecretString'])['engine']
                self.__username = json.loads(get_secret_value_response['SecretString'])['username']
                self.__password = json.loads(get_secret_value_response['SecretString'])['password']
                self.__host = json.loads(get_secret_value_response['SecretString'])['host']
                self.__port = json.loads(get_secret_value_response['SecretString'])['port']
                self.__db = json.loads(get_secret_value_response['SecretString'])['dbname']
                self.__token = json.loads(get_secret_value_response['SecretString'])['token']
            else:
                decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])
                return decoded_binary_secret
    
    def get_token(self):
        return self.__token
    
    def get_engine_sqlalchemy(self):
        return f'{self.__engine}://{self.__username}:{self.__password}@{self.__host}:{self.__port}/{self.__db}'
        
